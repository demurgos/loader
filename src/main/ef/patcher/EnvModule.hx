package ef.patcher;

import ef.loader.Version;

class EnvModule {
  private var _isDebug: Bool;

  public function new(isDebug: Bool) {
    this._isDebug = isDebug;
  }

  public function getLoaderVersion(): String {
    return Version.get();
  }

  public function isDebug(): Bool {
    return this._isDebug;
  }
}
