package ef.patcher;

import tink.core.Error;

class ErrorModule {
  public function new() {
  }

  public function createError(message: String): Error {
    return new Error(message);
  }
}
