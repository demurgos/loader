package ef.patcher;

class LocalConsoleModule implements IConsoleModule {
  public function new() {
  }

  public function log(message: Dynamic): Void {
    trace(message);
  }

  public function warn(message: Dynamic): Void {
    trace("Warning:");
    trace(message);
  }

  public function error(message: Dynamic): Void {
    trace("Error:");
    trace(message);
  }

  public function print(message: Dynamic): Void {
    trace("NotImplemented: `console.print`");
  }

  public function clear(): Void {
    trace("NotImplemented: `console.clear`");
  }
}
