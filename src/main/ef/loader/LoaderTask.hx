package ef.loader;

import etwin.ds.Nil;
import orchestrator.IDependencies;
import orchestrator.Orchestrator;
import orchestrator.TaskRunner;
import orchestrator.Token;
import tink.core.Error;
import tink.core.Future;
import tink.core.Outcome;
import tink.CoreApi.Surprise;
import ef.loader.LoaderProgress;

class LoaderTask {
  private var runner: TaskRunner<Dynamic>;
  private var future: Surprise<Dynamic, Error>;
  private var progress: LoaderProgress;

  public function new(runner: TaskRunner<Dynamic>, progress: LoaderProgress) {
    this.runner = runner;
    this.future = runner.execute();
    this.progress = progress;
  }

  public function handle(f: Outcome<Dynamic, Error> -> Void) {
    this.future.handle(f);
  }

  public function getProgress(): LoaderProgress {
    return this.progress;
  }

  public function getTotalProgress(): Nil<Float> {
    var current: Int = 0;
    var total: Int = 0;

    this.progress.engine.map(v => {
      current += v.current;
      total += v.total;
    });
    this.progress.content.map(v => {
      current += v.current;
      total += v.total;
    });
    this.progress.contentI18n.map(v => {
      current += v.current;
      total += v.total;
    });
    this.progress.patcher.map(v => {
      current += v.current;
      total += v.total;
    });
    for (musicProgress in this.progress.musics) {
      current += musicProgress.current;
      total += musicProgress.total;
    }

    if (total == 0) {
      return Nil.none();
    } else {
      return Nil.some(current / total);
    }
  }
}
