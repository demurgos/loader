package ef.loader;

class Version {
  public static var VERSION(null, never): String = "5.1.2";

  public static function get(): String {
    return Version.VERSION;
  }
}
