package ef.loader.views;

import etwin.flash.MovieClip;
import etwin.flash.Stage;

class Background {
  var x:Float;
  var y:Float;
  var size:Float;
  var color1:Int;
  var color2:Int;

  public function new(x:Float, y:Float, size:Float) {
    this.x = x;
    this.y = y;
    this.size = size;
    this.color1 = 0x3f3f3f;
    this.color2 = 0x262626;
  }

  public function draw(mc:MovieClip) {
    mc.moveTo(0, 0);
    mc.beginFill(this.color1);
    mc.lineTo(Stage.width, 0);
    mc.lineTo(Stage.width, Stage.height);
    mc.lineTo(0, Stage.height);
    mc.lineTo(0, 0);
    mc.endFill();

    var pt1:{x:Float, y:Float};
    var pt2:{x:Float, y:Float};
    for (i in 0...9) {
      mc.moveTo(this.x, this.y);
      mc.beginFill(this.color2);
      pt1 = this.getPoint(-3 / 12 + (2 * i) / 18);
      pt2 = this.getPoint(-3 / 12 + (2 * i + 1) / 18);
      mc.lineTo(pt1.x, pt1.y);
      mc.lineTo(pt2.x, pt2.y);
      mc.lineTo(this.x, this.y);
      mc.endFill();
    }
  }

  private function getPoint(ratio:Float):{x:Float, y:Float} {
    var TAU:Float = 2 * Math.PI;
    var size = this.size;
    var x = this.x + size * Math.cos(ratio * TAU);
    var y = this.y + size * Math.sin(ratio * TAU);
    return {x: x, y: y};
  }
}
