package ef.loader.views;

import etwin.flash.MovieClip;

class LoaderBar {
  var x: Float;
  var y: Float;
  var w: Float;
  var h: Float;
  var padding: Float;
  var backColor: Int;
  var frontColor: Int;
  var ticks: Int;
  var ratio: Null<Float>;

  public function new(x: Float, y: Float, w: Float, h: Float) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.padding = 2;
    this.backColor = 0x6a100e;
    this.frontColor = 0xa81e22;
    this.ticks = 0;
    this.ratio = null;
  }

  public function update() {
    this.ticks += 1;
  }

  public function setRatio(ratio: Float) {
    this.ratio = ratio;
  }

  public function draw(mc: MovieClip) {
    mc.moveTo(this.x, this.y);
    mc.beginFill(this.backColor);
    mc.lineTo(this.x + this.w, this.y);
    mc.lineTo(this.x + this.w, this.y + this.h);
    mc.lineTo(this.x, this.y + this.h);
    mc.lineTo(this.x, this.y);
    mc.endFill();

    var inX1 = this.x + this.padding;
    var inX2 = this.x + this.w - this.padding;
    var inY1 = this.y + this.padding;
    var inY2 = this.y + this.h - this.padding;

    if (this.ratio == null) {
      var r: Float = Math.max(0, Math.min(1, (this.ticks % 100) / 100));

      var lr1: Float = Math.max(0, r - 0.5);
      var lr2: Float = r;
      var rr1: Float = Math.min(1, r + 1 - 0.5);
      var rr2: Float = 1;

      var lx1 = inX1 + lr1 * (inX2 - inX1);
      var lx2 = inX1 + lr2 * (inX2 - inX1);
      var rx1 = inX1 + rr1 * (inX2 - inX1);
      var rx2 = inX1 + rr2 * (inX2 - inX1);

      mc.moveTo(lx1, inY1);
      mc.beginFill(this.frontColor);
      mc.lineTo(lx2, inY1);
      mc.lineTo(lx2, inY2);
      mc.lineTo(lx1, inY2);
      mc.lineTo(lx1, inY1);
      mc.endFill();

      if (this.ticks >= 100) {
        mc.moveTo(rx1, inY1);
        mc.beginFill(this.frontColor);
        mc.lineTo(rx2, inY1);
        mc.lineTo(rx2, inY2);
        mc.lineTo(rx1, inY2);
        mc.lineTo(rx1, inY1);
        mc.endFill();
      }
    } else {
      var r = Math.max(0, Math.min(1, this.ratio));
      inX2 = inX1 + r * (inX2 - inX1);

      mc.moveTo(inX1, inY1);
      mc.beginFill(this.frontColor);
      mc.lineTo(inX2, inY1);
      mc.lineTo(inX2, inY2);
      mc.lineTo(inX1, inY2);
      mc.lineTo(inX1, inY1);
      mc.endFill();
    }
  }
}
