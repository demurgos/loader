package ef.loader.views;

import ef.loader.LoaderTask;
import ef.loader.views.LoaderBar;
import etwin.flash.MovieClip;
import Reflect;
import tink.core.Future;
import etwin.Obfu;

class Uberjudgement {
  public static var WIDTH: Float = 420;
  public static var HEIGHT: Float = 520;
  public static var LOGO_WIDTH: Float = 350;
  public static var LOGO_HEIGHT: Float = 350;

  public var mc: MovieClip;
  var loaderBar: LoaderBar;
  var logo: MovieClip;
  var back: Background;
  var version: Dynamic;

  var prog: Float = 0;
  var loaderTask: Null<LoaderTask>;
  var onEnterFrame: Void -> Void;

  public function new(mc: MovieClip) {
    this.mc = mc;
    this.loaderTask = null;

    var loaderBarWidth: Float = 200;
    var loaderBarX = Uberjudgement.WIDTH / 2 - loaderBarWidth / 2;
    this.loaderBar = new LoaderBar(loaderBarX, 360, loaderBarWidth, 20);
    this.loaderBar.draw(this.mc);

    this.logo = this.mc.attachMovie("efl__eye", "efl__eye", this.mc.getNextHighestDepth());
    this.logo._x = Uberjudgement.WIDTH / 2 - Uberjudgement.LOGO_WIDTH / 2;
    this.logo._y = Uberjudgement.LOGO_HEIGHT / 5;

    this.back = new Background(Uberjudgement.WIDTH / 2, Uberjudgement.HEIGHT / 2, Uberjudgement.HEIGHT);

    this.version = FlashStd.createTextField(this.mc, this.mc.getNextHighestDepth());
    this.version.text = "Loader v" + Version.get();
    this.version._x = 13;
    this.version._y = Uberjudgement.HEIGHT - 30;
    this.version._width = 200;
    this.version.selectable = false;
    this.version.textColor = 0xffffff;
    var versionFormat = new flash.TextFormat();
    versionFormat.font = Obfu.raw("_sans");
    this.version.setTextFormat(versionFormat);

    this.onEnterFrame = function() {
      this.main();
    }

    FlashStd.addEnterFrameListener(this.onEnterFrame);
  }

  public function main() {
    this.update();
    this.mc.clear();
    this.draw();
  }

  public function update() {
    if (this.loaderTask != null) {
      var totalProgress: Null<Float> = this.loaderTask.getTotalProgress().toNullable();
      this.loaderBar.setRatio(totalProgress);
    }
    this.loaderBar.update();
  }

  public function draw() {
    this.back.draw(this.mc);
    this.loaderBar.draw(this.mc);
  }

  public function setLoaderTask(loaderTask: LoaderTask): Void {
    this.loaderTask = loaderTask;
  }

  public function setGameEnd(): Void {
    this.loaderBar.setRatio(1);
  }

  public function awaitStartGameSignal(): Future<Dynamic> {
    this.loaderBar.setRatio(1);
    return Future.async(function(done) {
      this.mc.onRelease = function() {
        Reflect.deleteField(this.mc, Obfu.raw("onRelease"));
        done(null);
      }
    });
  }

  public function destroy(): Void {
    FlashStd.removeEnterFrameListener(this.onEnterFrame);
    this.logo.removeMovieClip();
    this.mc.clear();
    this.loaderTask = null;
    this.back = null;
    this.loaderBar = null;
    this.onEnterFrame = null;
    this.version.removeTextField();
    this.version = null;
  }
}
