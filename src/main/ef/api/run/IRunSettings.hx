package ef.api.run;

interface IRunSettings {
  var sound: Bool;
  var music: Bool;
  var shake: Bool;
  var detail: Bool;
  var volume: Float;
}
