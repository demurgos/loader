package ef.api.run;

import ef.api.run.IRunOptions;
import ef.api.types.Ucs2String;
import tink.core.Error;
import etwin.Obfu;

class RunOptions implements IRunOptions {
  public var locale: String;

  public function new(locale: String) {
    this.locale = locale;
  }

  public static function readJson(raw: Dynamic): RunOptions {
    if (!Reflect.isObject(raw)) {
      throw new Error("Actual: " + Type.typeof(raw) + ", Expected: Object");
    }
    var locale: String = Ucs2String.readJson(Reflect.field(raw, Obfu.raw("locale")));
    return new RunOptions(locale);
  }
}
