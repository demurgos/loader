package ef.api.run;

import ef.api.run.IRunStart;
import ef.api.types.IdRef;
import ef.api.types.IIdRef;
import ef.api.types.Ucs2String;
import etwin.Obfu;
import tink.core.Error;

class RunStart implements IRunStart {
  public var run: IIdRef;
  public var startedAt: Date;
  public var families: String;
  public var items: Dynamic;
  public var key: String;

  public function new(run: IIdRef, startedAt: Date, families: String, items: Dynamic, key: String) {
    this.run = run;
    this.startedAt = startedAt;
    this.families = families;
    this.items = items;
    this.key = key;
  }

  public static function readJson(raw: Dynamic): RunStart {
    if (!Reflect.isObject(raw)) {
      throw new Error("Actual: " + Type.typeof(raw) + ", Expected: Object");
    }
    var run: IdRef = IdRef.readJson(Reflect.field(raw, Obfu.raw("run")));
    // TODO: Read from response
    // var startedAt:Date = DateType.readJson(Reflect.field(raw, Obfu.raw("started_at")));
    var startedAt: Date = Date.now();
    var families: String = Ucs2String.readJson(Reflect.field(raw, Obfu.raw("families")));
    var items: Dynamic = Reflect.field(raw, Obfu.raw("items"));
    var key: String = Ucs2String.readJson(Reflect.field(raw, Obfu.raw("key")));

    return new RunStart(run, startedAt, families, items, key);
  }
}
