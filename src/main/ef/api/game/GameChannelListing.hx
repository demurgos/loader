package ef.api.game;

import ef.api.user.ShortUser;
import ef.api.types.BooleanType;
import ef.api.types.IntType;
import ef.api.types.Ucs2String;
import ef.api.types.UuidHex;
import etwin.Obfu;
import Reflect;
import tink.core.Error;

class GameChannelListing {
  public var offset(default, null): Int;
  public var limit(default, null): Int;
  public var count(default, null): Int;
  public var isCountExact(default, null): Bool;
  public var active(default, null): ActiveGameChannel;
  public var items(default, null): Array<Dynamic>;

  public function new(
    offset: Int,
    limit: Int,
    count: Int,
    isCountExact: Bool,
    active: ActiveGameChannel,
    items: Array<Dynamic>
  ) {
    this.offset = offset;
    this.limit = limit;
    this.count = count;
    this.isCountExact = isCountExact;
    this.active = active;
    this.items = items;
  }

  public function toString(): String {
    return "[object GameChannelListing]";
  }

  public static function readJson(raw: Dynamic): GameChannelListing {
    if (!Reflect.isObject(raw)) {
      throw new Error("actual: " + Type.typeof(raw) + ", expected: Object");
    }
    var offset: Int = IntType.readJson(Obfu.field(raw, "offset"));
    var limit: Int = IntType.readJson(Obfu.field(raw, "limit"));
    var count: Int = IntType.readJson(Obfu.field(raw, "count"));
    var isCountExact: Bool = BooleanType.readJson(Obfu.field(raw, "is_count_exact"));
    var active: ActiveGameChannel = ActiveGameChannel.readJson(Obfu.field(raw, "active"));
    return new GameChannelListing(offset, limit, count, isCountExact, active, []);
  }
}
