package ef.api.game;

import ef.api.blob.Blob;
import ef.api.game.GameEngine.GameEngineTools;
import ef.api.types.ArrayType;
import ef.api.types.DateType;
import ef.api.types.NilType;
import ef.api.types.StringMapType;
import ef.api.types.Ucs2String;
import ef.api.types.UuidHex;
import etwin.ds.Nil;
import etwin.Obfu;
import Reflect;
import tink.core.Error;

class GameBuildI18n {
  public var displayName(default, null): Nil<String>;
  public var description(default, null): Nil<String>;
  public var icon(default, null): Nil<Blob>;
  public var contentI18n(default, null): Nil<Blob>;

  public function new(
    displayName: Nil<String>,
    description: Nil<String>,
    icon: Nil<Blob>,
    contentI18n: Nil<Blob>
  ) {
    this.displayName = displayName;
    this.description = description;
    this.icon = icon;
    this.contentI18n = contentI18n;
  }

  public function toString(): String {
    return "[object GameBuildI18n]";
  }

  public static function readJson(raw: Dynamic): GameBuildI18n {
    if (!Reflect.isObject(raw)) {
      throw new Error("actual: " + Type.typeof(raw) + ", expected: Object");
    }
    var displayName: Nil<String> = NilType.readJson(Obfu.field(raw, "display_name"), Ucs2String.readJson);
    var description: Nil<String> = NilType.readJson(Obfu.field(raw, "description"), Ucs2String.readJson);
    var icon: Nil<Blob> = NilType.readJson(Obfu.field(raw, "icon"), Blob.readJson);
    var contentI18n: Nil<Blob> = NilType.readJson(Obfu.field(raw, "content_i18n"), Blob.readJson);
    return new GameBuildI18n(displayName, description, icon, contentI18n);
  }
}
