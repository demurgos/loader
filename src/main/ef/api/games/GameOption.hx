package ef.api.games;

import ef.api.games.IGameOption;
import ef.api.types.Ucs2String;
import etwin.Obfu;
import tink.core.Error;

class GameOption implements IGameOption {
  public var id: String;
  public var state: GameOptionState;

  public function new(id: String, state: GameOptionState) {
    this.id = id;
    this.state = state;
  }

  public function toString(): String {
    return "[object GameOption]";
  }

  public static function readJson(raw: Dynamic): GameOption {
    if (!Reflect.isObject(raw)) {
      throw new Error("Actual: " + Type.typeof(raw) + ", Expected: Object");
    }
    var id: String = Ucs2String.readJson(Reflect.field(raw, Obfu.raw("key")));
    var stateStr: String = Ucs2String.readJson(Reflect.field(raw, Obfu.raw("state")));
    var state: GameOptionState = GameOption.deserializeGameOptionState(stateStr);
    return new GameOption(id, state);
  }

  public static function deserializeGameOptionState(optionState: String): GameOptionState {
    if (optionState == Obfu.raw("disabled")) {
      return GameOptionState.DISABLED;
    } else if (optionState == Obfu.raw("enabled")) {
      return GameOptionState.ENABLED;
    } else if (optionState == Obfu.raw("locked")) {
      return GameOptionState.LOCKED;
    } else {
      throw new Error("Unknown gameModeState: " + optionState);
    }
  }
}
