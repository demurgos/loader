package ef.api.games;

import ef.api.types.Ucs2String;
import etwin.Obfu;
import tink.core.Error;

class GameEngines implements IGameEngines {
  public var loader: String;
  public var game: Null<String>;
  public var external: Null<String>;
  public var eternaldev: Null<String>;

  public function new(loader: String, game: Null<String>, external: Null<String>, eternaldev: Null<String>) {
    this.loader = loader;
    this.game = game;
    this.external = external;
    this.eternaldev = eternaldev;
  }

  public function toString(): String {
    return "[object GameEngines]";
  }

  public static function readJson(raw: Dynamic): GameEngines {
    if (!Reflect.isObject(raw)) {
      throw new Error("Actual: " + Type.typeof(raw) + ", Expected: Object");
    }
    var loader: String = Ucs2String.readJson(Reflect.field(raw, Obfu.raw("loader")));
    var rawGame: Dynamic = Reflect.field(raw, Obfu.raw("game"));
    var game: Null<String> = rawGame != null ? Ucs2String.readJson(rawGame) : null;
    var rawExternal: Dynamic = Reflect.field(raw, Obfu.raw("external"));
    var external: Null<String> = rawGame != null ? Ucs2String.readJson(rawExternal) : null;
    var rawEternaldev: Dynamic = Reflect.field(raw, Obfu.raw("eternaldev"));
    var eternaldev: Null<String> = rawGame != null ? Ucs2String.readJson(rawEternaldev) : null;
    return new GameEngines(loader, game, external, eternaldev);
  }
}
