package ef.api.games;

import ef.api.games.IGameMode;
import ef.api.types.GameOptionArray;
import ef.api.types.Ucs2String;
import tink.core.Error;
import etwin.Obfu;

class GameMode implements IGameMode {
  public var id: String;
  public var state: GameModeState;
  public var options: Array<IGameOption>;

  public function new(id: String, state: GameModeState, options: Array<IGameOption>) {
    this.id = id;
    this.state = state;
    this.options = options;
  }

  public function toString(): String {
    return "[object GameMode]";
  }

  public static function readJson(raw: Dynamic): GameMode {
    if (!Reflect.isObject(raw)) {
      throw new Error("Actual: " + Type.typeof(raw) + ", Expected: Object");
    }
    var id: String = Ucs2String.readJson(Reflect.field(raw, Obfu.raw("key")));
    var stateStr: String = Ucs2String.readJson(Reflect.field(raw, Obfu.raw("state")));
    var state: GameModeState = GameMode.deserializeGameModeState(stateStr);
    var options: Array<IGameOption> = GameOptionArray.readJson(Reflect.field(raw, Obfu.raw("options")));
    return new GameMode(id, state, options);
  }

  public static function deserializeGameModeState(gameModeState: String): GameModeState {
    if (gameModeState == Obfu.raw("disabled")) {
      return GameModeState.DISABLED;
    } else if (gameModeState == Obfu.raw("enabled")) {
      return GameModeState.ENABLED;
    } else {
      throw new Error("Unknown gameModeState: " + gameModeState);
    }
  }
}
