package ef.api.users;

import ef.api.types.Ucs2String;
import ef.api.types.UuidHex;
import Reflect;
import tink.core.Error;
import etwin.Obfu;

class UserRef implements IUserRef {
  public var id:String;
  public var displayName:String;

  public function new(
    id:String,
    displayName:String
  ) {
    this.id = id;
    this.displayName = displayName;
  }

  public function toString():String {
    return "[UserRef@" + this.id + "]";
  }

  public static function readJson(raw:Dynamic):UserRef {
    if (!Reflect.isObject(raw)) {
      throw new Error("Actual: " + Type.typeof(raw) + ", Expected: Object");
    }
    var id:String = UuidHex.readJson(Reflect.field(raw, Obfu.raw("id")));
    var displayName:String = Ucs2String.readJson(Reflect.field(raw, Obfu.raw("display_name")));
    return new UserRef(id, displayName);
  }
}
