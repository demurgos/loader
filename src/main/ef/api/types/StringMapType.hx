package ef.api.types;

import etwin.Error;

class StringMapType {
  public static function readJson<T>(raw: Dynamic, reader: Dynamic -> T): Map<String, T> {
    if (!Reflect.isObject(raw)) {
      throw new Error("actual: " + Type.typeof(raw) + ", expected: Object");
    }
    var result: Map<String, T> = new Map();
    for (key in Reflect.fields(raw)) {
      result[key] = reader(Reflect.field(raw, key));
    }
    return result;
  }
}
