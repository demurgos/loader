package ef.api.types;
import tink.core.Error;

class IntType {
  public static function readJson(raw: Dynamic): Int {
    if (!Std.is(raw, Int)) {
      throw new Error("Actual: " + Type.typeof(raw) + ", Expected: Integer");
    }
    return raw;
  }

  public static function testError(raw: Dynamic): Null<Error> {
    return null;
  }
}
