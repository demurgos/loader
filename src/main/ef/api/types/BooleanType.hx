package ef.api.types;
import tink.core.Error;

class BooleanType {
  public static function readJson(raw:Dynamic):Bool {
    if (!Std.is(raw, Bool)) {
      throw new Error("Actual: " + Type.typeof(raw) + ", Expected: Boolean");
    }
    return raw;
  }

  public static function testError(raw:Dynamic):Null<Error> {
    return null;
  }
}
