package ef.api.types;
import tink.core.Error;

class UuidHex {
  public static function readJson(raw:Dynamic):String {
    if (!Std.is(raw, String)) {
      throw new Error("Actual: " + Type.typeof(raw) + ", Expected: String");
    }
    var err:Null<Error> = UuidHex.testError(raw);
    if (err != null) {
      throw err;
    }
    return raw;
  }

  public static function testError(raw:Dynamic):Null<Error> {
    return null;
  }
}
