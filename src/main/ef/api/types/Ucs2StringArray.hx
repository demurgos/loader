package ef.api.types;

import tink.core.Error;

class Ucs2StringArray {
  public static function readJson(raw:Dynamic):Array<String> {
    if (!Std.is(raw, Array)) {
      throw new Error("Actual: " + Type.typeof(raw) + ", Expected: Array");
    }
    var result:Array<String> = [];
    for (i in 0...raw.length) {
      result.push(Ucs2String.readJson(raw[i]));
    }
    return result;
  }

  public static function testError(raw:Dynamic):Null<Error> {
    return null;
  }
}
