package ef.api;

import etwin.Obfu;
import net.demurgos.uri.Uri;

class ApiRoutes {
  public var baseUri: Uri;

  public function new(baseUri: Uri) {
    this.baseUri = baseUri;
  }

  private function gameParts(gameId: String): Array<String> {
    return [Obfu.raw("games"), gameId];
  }

  private function runStartParts(runId: String): Array<String> {
    return [Obfu.raw("runs"), runId, Obfu.raw("start")];
  }

  private function runResultParts(runId: String): Array<String> {
    return [Obfu.raw("runs"), runId, Obfu.raw("result")];
  }

  private function rawBlobParts(fileId: String): Array<String> {
    return [Obfu.raw("blobs"), fileId, Obfu.raw("raw")];
  }

  private function rawFileParts(fileId: String): Array<String> {
    return [Obfu.raw("files"), fileId, Obfu.raw("raw")];
  }

  private function gameFileParts(): Array<String> {
    return [Obfu.raw("assets"), Obfu.raw("game.swf")];
  }

  public function gameUri(gameId: String): Uri {
    var uriString: String = this.baseUri.toString();
    var path = this.baseUri.getPath();
    if (path.lastIndexOf("/") != path.length - 1) {
      uriString += "/";
    }
    return new Uri(uriString + this.gameParts(gameId).join("/"));
  }

  public function runStartUri(runId: String): Uri {
    var uriString: String = this.baseUri.toString();
    var path = this.baseUri.getPath();
    if (path.lastIndexOf("/") != path.length - 1) {
      uriString += "/";
    }
    return new Uri(uriString + this.runStartParts(runId).join("/"));
  }

  public function runResultUri(runId: String): Uri {
    var uriString: String = this.baseUri.toString();
    var path = this.baseUri.getPath();
    if (path.lastIndexOf("/") != path.length - 1) {
      uriString += "/";
    }
    return new Uri(uriString + this.runResultParts(runId).join("/"));
  }

  public function rawBlob(blobId: String): Uri {
    var uriString: String = this.baseUri.toString();
    var path = this.baseUri.getPath();
    if (path.lastIndexOf("/") != path.length - 1) {
      uriString += "/";
    }
    return new Uri(uriString + this.rawBlobParts(blobId).join("/"));
  }

  public function rawFile(fileId: String): Uri {
    var uriString: String = this.baseUri.toString();
    var path = this.baseUri.getPath();
    if (path.lastIndexOf("/") != path.length - 1) {
      uriString += "/";
    }
    return new Uri(uriString + this.rawFileParts(fileId).join("/"));
  }

  public function gameFile(): Uri {
    var originString: String = this.baseUri.getOrigin();
    if (originString.lastIndexOf("/") != originString.length - 1) {
      originString += "/";
    }
    return new Uri(originString + this.gameFileParts().join("/"));
  }
}
