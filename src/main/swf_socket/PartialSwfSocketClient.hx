package swf_socket;

import etwin.Obfu;
import flash.external.ExternalInterface;

class PartialSwfSocketClient {
  public var objectId: String;
  public var clientMethod: String;

  public function new(objectId: String, clientMethod: String) {
    this.objectId = objectId;
    this.clientMethod = clientMethod;
#if flash8
    ExternalInterface.addCallback(clientMethod, this, this.onMessage);
#else
    ExternalInterface.addCallback(clientMethod, this.onMessage);
#end
  }

  public function onMessage(message: Dynamic): Dynamic {
    var raw: Dynamic = {};
    Reflect.setField(raw, Obfu.raw("complete"), false);
    return raw;
  }

  public function complete(remoteMethod: String): SwfSocketClient {
    return new SwfSocketClient(this.objectId, this.clientMethod, remoteMethod);
  }
}
