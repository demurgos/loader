package orchestrator;

interface IDependencies {
  function get<T>(token: Token<T>): Null<T>;
}
