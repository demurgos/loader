package orchestrator;

import tink.core.Error;
import tink.core.Future;
import tink.core.Outcome;
import tink.CoreApi.Surprise;

class AbstractTask<T> {
  public var token(default, null):Token<Dynamic>;
  public var name(default, null):String;

  private function new(token:Token<Dynamic>, name:String) {
    this.token = token;
    this.name = name;
  }

  public function runAsync(dependencies:IDependencies):Surprise<T, Error> {
    return Future.sync(Outcome.Failure(new Error("Called abstract method AbstractTask:runAsync")));
  }
}
