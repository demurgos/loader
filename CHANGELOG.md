# 5.1.2 (2023-11-10)

- **[Fix]** Remove executable flag from `.gitattributes`
- **[Internal]** Remove executable flag from `.gitattributes`
- **[Internal]** Update to Yarn 4

# 5.1.1 (2023-02-14)

- **[Fix]** Use run API v2.

# 5.1.0 (2023-02-10)

- **[Feature]** Support using a locale different from the main locale.
- **[Fix]** Use game API v2.

# 5.0.1 (2023-01-31)

- **[Fix]** Update progress on load complete for movie clips and sounds.

# 5.0.0 (2023-01-31)

- **[Breaking change]** Require Game API v2
- **[Breaking change]** Remove deprecated functions from `loader-types`.
- **[Feature]** Display download progress

# 4.1.0 (2022-02-18)

- **[Breaking change]** Migrate to native ESM packages.
- **[Feature]** Catch and log errors thrown by the game.
- **[Fix]** Use `etwin` Haxe packages for `Obfu` and core types.
- **[Fix]** Use global `URL` type.
- **[Fix]** Update dependencies.

# 4.0.2 (2020-08-27)

- **[Fix]** Update dependencies, update to `game-types@0.5.2`.

# 4.0.1 (2020-04-21)

- **[Fix]** Fix `Std.createTextField` function.
- **[Fix]** Update Open Flash dependencies.

# 4.0.0 (2020-01-28)

- **[Breaking change]** Require level set signatures.
- **[Breaking change]** Remove support for `external1` and `external2`.
- **[Breaking change]** Return `URL` in Typescript API.
- **[Fix]** Update dependencies.
- **[Internal]** Fix continuous deployment script.

# 3.8.1 (2019-12-31)

- **[Internal]** Update build system.

# 3.8.0 (2019-12-23)

- **[Feature]** Add `endGame` to `game-events` module.

# 3.7.2 (2019-12-19)

- **[Feature]** Expose obfu map URI.
- **[Fix]** Update list of protected identifiers.
- **[Fix]** Fix compat for `GameManager` options.

# 3.7.1 (2019-12-04)

- **[Internal]** Fix obfuscation issues with: `Array.prototype.insert`, `Array.prototype.remove` and `isFinite`.
- **[Internal]** Update Docker image used for CI.

# 3.7.0 (2019-11-28)

- **[Fix]** Obfuscate loader.

# 3.6.0 (2019-09-05)

- **[Feature]** Add support for level set signatures.
- **[Fix]** Add deprecation warnings for old external versions.

# 3.5.3 (2019-09-03)

- **[Fix]** Fix obfuscated dimension names.

# 3.5.2 (2019-08-23)

- **[Feature]** Add support for games without a `game-swf` resource.
- **[Fix]** Fix `run-start` initialization.

# 3.5.1 (2019-08-23)

- **[Fix]** Fix support for game content injection in obfuscated game.

# 3.5.0 (2019-08-22)

- **[Feature]** Add `run-start` patcher module.

# 3.4.1 (2019-06-20)

- **[Fix]** Ensure game end events are called only once.

# 3.4.0 (2019-05-18)

- **[Feature]** Update to `api-core@0.9`.
- **[Fix]** Redirect to run result instead of game list on game end.

# 3.3.2 (2019-04-14)

- **[Fix]** Fix game-end support for games like `eternel-recommencement`

# 3.3.1 (2019-03-31)

- **[Feature]** Load run options from the server
- **[Fix]** Fix passing data for the first score and special item families

# 3.3.0 (2019-02-05)

- **[Fix]** Implement `.print` and `.clear` for `SwfSocketConsoleModule` ([#2](https://gitlab.com/eternalfest/loader/issues/2))
- **[Fix]** Add support for direct accesses to `_global` (fixes `hud_label` duration).
- **[Internal]** Update docker image for CI ([#1](https://gitlab.com/eternalfest/loader/issues/1))

# 3.2.2 (2019-01-25)

- **[Fix]** Compress Flash 8 loader.

# 3.2.1 (2019-01-23)

- **[Fix]** Use main logo for loader view.
- **[Fix]** Use black background.
- **[Internal]** Use Typescript for build scripts.

# 3.2.0 (2019-01-03)

- **[Feature]** Add full support for game results.
- **[Fix]** Fix end game patches (add support for "La Faille Éternelle).

# 3.1.0 (2018-12-31)

- **[Feature]** Add fade away effect on game end.
- **[Feature]** Get run start data (including families) from server.
- **[Feature]** Add support for run results.
- **[Fix]** Fix internal game end function patch.

# 3.0.4 (2018-12-24)

- **[Fix]** Fix console module selection for engine patcher

# 3.0.3 (2018-12-24)

- **[Fix]** Use `NoopConsoleModule` in production.
- **[Fix]** Fix loader status.
- **[Fix]** Fix end game support.

# 3.0.2 (2018-12-24)

- **[Fix]** Parse dates.
- **[Fix]** Fix double updates (80FPS) with External2.
- **[Fix]** Increase music loading timeout to 10min.
- **[Fix]** Restrict debug mode to `localhost`.

# 3.0.1 (2018-12-20)

- **[Fix]** Use snake case when reading `File` resources.

# 3.0.0 (2018-12-17)

- **[Feature]** Add support for the `eternalfest.net` API.
- **[Internal]** Update CI script.
- **[Internal]** Update dependencies.

# 3.0.0-beta.2 (2018-09-04)

- **[Fix]** Fix support for External mods relying on local dev mode.
- **[Internal]** Reformat code.

# 3.0.0-beta.1 (2017-07-23)

- **[Feature]** Add support for bidirectional communication with Javascript, called "SWF Socket"
- **[Feature]** Add runtime debugger server to inspect values.
- **[Feature]** Inject `hxTrace` for debug purposes.
- **[Fix]** Set default color of the Flash logger to yellow (`#ffff00`).
- **[Fix]** Add `Log.clear` as an alias for `Log.clean`.
- **[Fix]** Expose the features provided by the `StdInc` module as both `HfestStd` and `Std`.
  It was previously exposed only as `Std`, this caused name conflicts when compiling the
  game with Haxe.

# 3.0.0-beta.0 (2017-07-02)

- **[Feature]** Add support for run options (mode, options, settings).
- **[Feature]** Implement SWF sockets to connect to the host environment. These allow to
  use a remote console for the engine patcher to log messages in the browser.

# 3.0.0-alpha.4 (2017-06-26)

- **[Fix]** Fix support for games requiring deobfuscated Manager options
- **[Fix]** Fix levels hash computation

# 3.0.0-alpha.3 (2017-06-22)

- **[Breaking]** Differentiate engine patchers: `external1`, `external2` and `engine-patcher`
  (instead of having a single `es-swf` tag).
- **[Feature]** Add support for engine patchers following v3.0.0
- **[Feature]** Apply level hashes while content-patching the game engine

# 3.0.0-alpha.2 (2017-06-21)

- **[Internal]** Create `CHANGELOG.md`
