import fsExtra from "fs-extra";
import * as fs from "fs";
import * as gulp from "gulp";
import * as sysPath from "path";
import { exec } from "./gulp/exec";
import { getHaxeDependencies } from "./gulp/get-haxe-dependencies";
import { haxelibInstallDependencies } from "./gulp/haxelib-install";
import { parseSwf } from "swf-parser";
import { emitSwf } from "swf-emitter";
import { CompressionMethod, Movie } from "swf-types";
import { swfMerge } from "swf-merge";
import { obfuscate } from "./gulp/obfu";
import { mkdirpAsync, outputFileAsync, readFile } from "./gulp/fs-utils";
import { fromSysPath, Furi, join as furiJoin } from "furi";

const PROJECT_ROOT: Furi = new Furi(fromSysPath(__dirname));

gulp.task("haxelib:install", () => {
  return haxelibInstallDependencies(getHaxeDependencies());
});

async function buildFlash8Code(): Promise<void> {
  await mkdirpAsync(furiJoin(PROJECT_ROOT, "build", "flash8", "tmp"));
  await exec("haxe", ["loader.main.flash8.hxml"], {cwd: __dirname});
}

async function buildFlash8Assets(): Promise<void> {
  await mkdirpAsync(furiJoin(PROJECT_ROOT, "build", "flash8", "tmp"));
  await exec("swfmill", ["simple", "assets.xml", "build/flash8/tmp/assets.swf"], {cwd: PROJECT_ROOT.toSysPath()});
}

async function mergeFlash8Assets(): Promise<void> {
  const game: Movie = await readSwf(furiJoin(PROJECT_ROOT, ["build", "flash8", "tmp", "code.swf"]));
  const assets: Movie = await readSwf(furiJoin(PROJECT_ROOT, ["build", "flash8", "tmp", "assets.swf"]));
  const merged: Movie = swfMerge(game, assets);
  await outputSwf(furiJoin(PROJECT_ROOT, ["build", "flash8", "tmp", "raw.swf"]), merged);
}

function generateBuildFlash8Map(libDir: string, typesDir: string): gulp.TaskFunction {
  const task: gulp.TaskFunction = async function obfuscateFlash8(): Promise<void> {
    const rawMovieUrl: URL = furiJoin(PROJECT_ROOT, ["build", "flash8", "tmp", "raw.swf"]);
    const mappedMovieUrl: URL = furiJoin(PROJECT_ROOT, libDir, ["loader.flash8.swf"]);
    const mapUrl: URL = furiJoin(PROJECT_ROOT, typesDir, ["loader.map.json"]);
    await obfuscate(rawMovieUrl, mappedMovieUrl, mapUrl, "loader");
  };
  task.displayName = "flash8:build:map";
  return task;
}

gulp.task("flash8:build:raw", gulp.series([
  gulp.parallel([buildFlash8Code, buildFlash8Assets]),
  mergeFlash8Assets,
]));

gulp.task("flash8:build", gulp.series([
  gulp.task("flash8:build:raw"),
  generateBuildFlash8Map("build/lib", "build/lib-types"),
]));

gulp.task("flash8:dist", gulp.series([
  gulp.task("flash8:build:raw"),
  generateBuildFlash8Map("dist/lib", "dist/lib-types"),
]));

gulp.task("flash8:build:code", buildFlash8Code);

gulp.task("flash8:build:assets", buildFlash8Assets);

gulp.task(
  "flash11:build",
  () => exec("haxelib", ["run", "openfl", "build", "loader.main.flash11.lime", "flash"], {cwd: __dirname})
);

// haxelib run munit run -browser "xvfb-run firefox -profile firefox-profile" -kill-browser -result-exit-code -as3
// echo "------------------------------"
// echo "Summary for as3:"
// cat build/report/test/summary/as3/summary.txt
// echo "------------------------------"


gulp.task(
  "flash11-test:build:_munit",
  () => exec("haxelib", ["run", "munit", "gen", "src/test"], {cwd: __dirname})
);

gulp.task(
  "flash11-test:build:_openfl",
  () => exec("haxelib", ["run", "openfl", "build", "loader.test.flash11.lime", "flash"], {cwd: __dirname})
);

gulp.task(
  "flash11-test:build:_firefox",
  () => {
    return fsExtra.emptyDir(sysPath.resolve(__dirname, "build", "test", "firefox-profile"))
      .then(() => {
        return fsExtra.copy(
          sysPath.resolve(__dirname, "gulp", "firefox-profile-template"),
          sysPath.resolve(__dirname, "build", "test", "firefox-profile"),
        );
      });
  }
);

gulp.task(
  "flash11-test:run",
  () => exec(
    "haxelib",
    [
      "run", "munit",
      "run",
      "-browser", "xvfb-run firefox-esr68 -profile firefox-profile",
      "-kill-browser",
      "-result-exit-code",
      "-as3",
      sysPath.resolve(__dirname, "build", "test", "flash", "bin"),
    ],
    {cwd: __dirname}
  )
);

gulp.task(
  "flash11-test:build",
  gulp.parallel(
    gulp.series("flash11-test:build:_munit", "flash11-test:build:_openfl"),
    "flash11-test:build:_firefox"
  )
);


gulp.task("flash11-test", gulp.series("flash11-test:build", "flash11-test:run"));

gulp.task("all:build", gulp.parallel("flash8:build", "flash11:build"));
gulp.task("all:test", gulp.parallel("flash11-test"));

async function readSwf(filePath: fs.PathLike): Promise<Movie> {
  const buffer: Uint8Array = await readFile(filePath);
  return parseSwf(buffer);
}

async function outputSwf(filePath: URL, movie: Movie): Promise<void> {
  // TODO: Use `CompressionMethod.Deflate`
  // Using the constant 1 is a temporary workaround
  const bytes: Uint8Array = emitSwf(movie, 1 as CompressionMethod);
  return outputFileAsync(filePath, bytes);
}
