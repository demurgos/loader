import * as cp from "child_process";

export async function haxelibInstall(name: string, version: string): Promise<void> {
  return new Promise((resolve, reject) => {
    console.log(`Installing Haxe library: ${name}@${version}`);
    const process = cp.spawn(
      "haxelib",
      ["install", name, version],
      {
        stdio: "inherit"
      }
    );
    process.once("exit", (code, _signal) => {
      if (code !== 0) {
        reject(new Error("Haxe installation did not exit with a 0 code"));
      } else {
        resolve();
      }
    })
  });
}

export async function haxelibInstallDependencies(dependencies: Record<string, string>): Promise<void> {
  for (const name in dependencies) {
    await haxelibInstall(name, dependencies[name]);
  }
}
