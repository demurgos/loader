import { getLoaderUri, Version } from "../lib/index.mjs";
import chai from "chai";
import { existsSync } from "fs";

describe("loader", function () {
  it("supports Flash 8", function() {
    const loader = getLoaderUri(Version.Flash8);
    chai.assert.instanceOf(loader, URL);
    chai.assert(existsSync(loader), `file exists: ${loader}`);
  });

  it("supports Flash 11", function() {
    const loader = getLoaderUri(Version.Flash11);
    chai.assert.instanceOf(loader, URL);
    chai.assert(existsSync(loader), `file exists: ${loader}`);
  });
});
