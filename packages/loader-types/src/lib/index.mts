import sysPath from "path";
import { pathToFileURL, fileURLToPath } from "url";

export const FLASH8_OBFU = toUri("loader.map.json");
export const VERSION = "5.1.2";

export function getObfuMapUri(): URL {
  return FLASH8_OBFU;
}

function toUri(filename: string): URL {
  const selfPath = fileURLToPath(import.meta.url);
  const resultPath = sysPath.join(selfPath, "..", filename);
  return pathToFileURL(resultPath);
}
